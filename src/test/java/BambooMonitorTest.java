import org.junit.Assert;
import org.junit.Test;

public class BambooMonitorTest {

    @Test
    public void test(){
        Assert.assertEquals("Bamboo Monitor is up and running!", BambooMonitor.printMessage());
    }

}
