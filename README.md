# Bamboo Monitor - Hackathon

## Sample Java app to test the Bamboo builds.

The app has two main branches for the success and fail scenarios:

- master (success case)
- failing_build (obviously the failing case)